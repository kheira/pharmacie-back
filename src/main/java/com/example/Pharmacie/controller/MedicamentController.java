package com.example.Pharmacie.controller;

import com.example.Pharmacie.model.Fournisseur;
import com.example.Pharmacie.model.Medicament;
import com.example.Pharmacie.repository.MedicamentRepository;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@CrossOrigin
@RestController
public class MedicamentController {

    private  List<Medicament> medicaments = new ArrayList<Medicament> ();
    @Autowired
    private MedicamentRepository medRepository;
    @Autowired
    MongoTemplate mongoTemplate;


    // ajouter un medicament
    @PostMapping("/addMedicament")
    public String saveMedicament(@RequestBody Medicament med){
        String nameFour = med.getFournisseur().getNomFournisseur();
        Query query = new Query(new Criteria("nomFournisseur").is(nameFour));
        Fournisseur result = mongoTemplate.findOne(query, Fournisseur.class);
        med.setFournisseur(result);
        medRepository.save(med);
        return "Médicament ajouté avec succes :: "+med.getCodeBarre();

    }
    // la liste de tous les medicaments
    @GetMapping("/medicaments")
    public List<Medicament> getListOfMédicaments() {
        return medRepository.findAll();
    }
    // nombre des medicaments présent dans le stock
    @GetMapping("/countmedicaments")
    public  long countMédicaments() {

        return medRepository.count();
    }

    //Récuperer un medicament avec son code barre
    @GetMapping("/medicaments/{codeBarre}")
    public Medicament getMedBycode(@PathVariable(value = "codeBarre") String codeBarre){
        medicaments = medRepository.findAll();
        for(Medicament medicament: medicaments){
            if(medicament.getCodeBarre().equals(codeBarre)){
                return medicament;
            }

        }
        return null;
    }

    // Liste des medicaments avec Quantité minimal
    @GetMapping("/quantiteMin")
    public List<Medicament> quantMin() {
        medicaments = medRepository.findAll();
        List<Medicament> medMin = new ArrayList<Medicament>();
        for (Medicament medicament : medicaments) {
            if (medicament.getQuantite() <= 10){
                medMin.add(medicament);
            }
        }
        return medMin;
    }


    // Nombre des medicaments avec Quantité minimal
    @GetMapping("/nbQuantiteMin")
    public int nbQuantMin() {
        medicaments = medRepository.findAll();
        List<Medicament> medMin = new ArrayList<Medicament>();
        int count = 0;
        for (Medicament medicament : medicaments) {
            if (medicament.getQuantite() <= 10){
                count++;
            }
        }
        return count;
    }

    @GetMapping("/medExp")
    public List<Medicament> medExp() {
        medicaments = medRepository.findAll();
        List<Medicament> medExp = new ArrayList<Medicament>();
        Date today = new Date();
        for (Medicament medicament : medicaments) {
            if (medicament.getDatePeremption().before(today)){
                medExp.add(medicament);
            }
        }
        return medExp;
    }

    //nombre des medicaments expirés
    @GetMapping("/nbrMedExp")
    public int NombremedExp() {
        medicaments = medRepository.findAll();
        List<Medicament> medExp = new ArrayList<Medicament>();
        Date today = new Date();
        int count = 0;
        for (Medicament medicament : medicaments) {
            if (medicament.getDatePeremption().before(today)){
                count++;
            }
        }
        return count;
    }

    //supprimé un médicament
//Utilisation d'une autre methode pour acceder aux données (mongoTemplate)

    @DeleteMapping("/medicaments/{codeBarre}")
    public void deleteMed(@PathVariable(value = "codeBarre") String codeBarre){
        Query query = new Query(new Criteria("codeBarre").is(codeBarre));
        DeleteResult result = mongoTemplate.remove(query, Medicament.class);
        System.out.println("Deleted documents: " + result.getDeletedCount());
    }

    // modifier un médicament
// Utilisation d'une autre methode (mongoTemplate)
    @PutMapping("/medicaments/{codeBarre}")
    public String updateMed(@RequestBody Medicament medicament, @PathVariable(value = "codeBarre") String codeBarre){
        Query query = new Query(new Criteria("codeBarre").is(codeBarre));
        Update update = new Update().set("quantite", medicament.getQuantite())
                .set("datePeremption", medicament.getDatePeremption())
                .set("fournisseur",medicament.getFournisseur())
                ;
        UpdateResult result = mongoTemplate.updateFirst(query, update, Medicament.class);
        return "Médicament mis à jour avec succes ";
    }







}