package com.example.Pharmacie.controller;

import com.example.Pharmacie.model.Commande;
import com.example.Pharmacie.model.Fournisseur;
import com.example.Pharmacie.model.Medicament;
import com.example.Pharmacie.repository.CommandeRepository;
import com.example.Pharmacie.repository.FournisseurRepository;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class FournisseurController {

    @Autowired
    private FournisseurRepository fournisseurRepository;
    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private CommandeRepository cmdRepository;
    @Autowired
    MongoTemplate mongoTemplate;

    @PostMapping("/addFournisseur")
    public String saveFournisseur(@RequestBody Fournisseur fournisseur){
        fournisseurRepository.save(fournisseur);
        return "Fournisseur ajouté avec succes :: "+fournisseur.getNomFournisseur();
    }
    @GetMapping("/fournisseurs")
    public List<Fournisseur> getListFournisseur(){
        return fournisseurRepository.findAll();
    }

    //contacter le fournisseur
    @PostMapping("fournisseurs/{id}")
    public String contacterfournisseurList(@RequestBody String body ,@PathVariable(value = "id") int id){
        Query query = new Query(new Criteria("idFournisseur").is(id));
        Fournisseur fournisseur = mongoTemplate.findOne(query, Fournisseur.class);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(fournisseur.getMail());
        message.setSubject("Demande de renseignements - Pharmacie miage ");
        message.setText(body);
        emailSender.send(message);
        return "le message a bien été envoyé au fournisseur";

    }
    //contacter le fournisseur à partir de liste commande
    @PostMapping("commandes/contact/{id}")
    public String contacterfournisseur(@RequestBody String body, @PathVariable(value = "id") int id){
        Commande cmd = cmdRepository.findById(id).orElseThrow();
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(cmd.getMedicament().getFournisseur().getMail());
        message.setSubject("Commande N°" + cmd.getIdCommande());
        message.setText(body);
        emailSender.send(message);
        return "le message a bien été envoyé au fournisseur";

    }



}
