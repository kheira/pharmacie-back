package com.example.Pharmacie.controller;

import com.example.Pharmacie.model.Commande;
import com.example.Pharmacie.model.Statut;
import com.example.Pharmacie.repository.CommandeRepository;
import com.example.Pharmacie.repository.StatutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class StatutController {
    @Autowired
    private StatutRepository statutRepository;


    @PostMapping("/addStatut")
    public String saveStatut(@RequestBody Statut statut){
        statutRepository.save(statut);
        return "Statut ajouté avec succes :: "+statut.getStatutCommande();
    }
}
