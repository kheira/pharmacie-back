package com.example.Pharmacie.controller;


import com.example.Pharmacie.model.Commande;
import com.example.Pharmacie.model.Fournisseur;
import com.example.Pharmacie.model.Medicament;
import com.example.Pharmacie.model.Statut;
import com.example.Pharmacie.repository.CommandeRepository;
import com.example.Pharmacie.repository.StatutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class CommandeController {

    @Autowired
    private CommandeRepository cmdRepository;
    @Autowired
    private StatutRepository statutRepository;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private JavaMailSender emailSender;

    // ajouter une Commande
    @PostMapping("/commandes/{codeBarre}")
    public String saveCommande(@RequestBody Commande cmd, @PathVariable (value = "codeBarre") String codeBarre) {
        Query query = new Query(new Criteria("codeBarre").is(codeBarre));
        Medicament result = mongoTemplate.findOne(query, Medicament.class);

        cmd.setMedicament(result);
        Date today = new Date();
        cmd.setDateCommande(today);
        cmd.setMontant((result.getPrix()) * (cmd.getNbrMed()));

        // Ne pas commander un médicament déja en commande
        List<Commande> commandes = cmdRepository.findAll();
        Statut st = statutRepository.findById(2).orElseThrow();
        for (Commande commande : commandes) {

            if (commande.getMedicament().getCodeBarre().equals(result.getCodeBarre())) {
                return "Le médicament " + result.getNom() + " a déja été commandé";
            } else {
                cmd.setStatut(st);
                cmdRepository.save(cmd);
                // envoie de mail
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(result.getFournisseur().getMail());
                message.setSubject("Commande N°" + cmd.getIdCommande());
                message.setText("Bonjour, \n \n Nous vous passons la commande suivante :\n Nom :"
                        + result.getNom() + "\n Code barre : "
                        + result.getCodeBarre() + "\n Quantité :"
                        + cmd.getNbrMed() + "\n"

                        + "_____________________________\n"
                        + "Total commande : " + cmd.getMontant()
                );
                emailSender.send(message);


                return "Commande ajouté avec succes :: " + cmd.getIdCommande();
            }
        }
        return null;
    }
    //nmbr total de commande
    @GetMapping("/countCommandes")
    public  long countMédicaments() {

        return cmdRepository.count();
    }
    //List des commandes
    @GetMapping("/commandes")
    public List<Commande> getListOfCommande() {
        return cmdRepository.findAll();
    }
    //get commande by id

    @GetMapping("/commandes/{id}")
    public Optional<Commande> getCommande(@PathVariable(value = "id") int id) {
        return cmdRepository.findById(id);
    }
    //supp une commande
    @DeleteMapping("/commandes/{id}")
    public void deleteommande(@PathVariable(value = "id") int id) {
         cmdRepository.deleteById(id);
    }
    // Quand la commande est reçue
    @PutMapping("/commandes/{id}")
    public Commande changeToReçu(@RequestBody Commande cmd, @PathVariable(value = "id") int id){
        Statut st = statutRepository.findById(1).orElseThrow();
        return cmdRepository.findById(id)
                .map(commande -> {
                    commande.setStatut(st);
                    return cmdRepository.save(commande);
                }).orElseGet(() -> {
                    cmd.setIdCommande(id);

                     return  cmdRepository.save(cmd);

                });
    }
    //Quand une commande est saisie
    @PutMapping("/commandesReçu/{id}")
    public Commande changeToSaisi(@RequestBody Commande cmd, @PathVariable(value = "id") int id){
        Statut st = statutRepository.findById(3).orElseThrow();
        return cmdRepository.findById(id)
                .map(commande -> {
                    commande.setStatut(st);
                    return cmdRepository.save(commande);
                }).orElseGet(() -> {
                    cmd.setIdCommande(id);

                    return  cmdRepository.save(cmd);

                });
    }

    //liste des commandes reçue
    @GetMapping("/commandesReçu")
    public List<Commande> getCommandesReçu() {


        List<Commande>  commandes = cmdRepository.findAll();
        List<Commande> reçu = new ArrayList<Commande>();
        for (Commande commande : commandes) {
            if (commande.getStatut().getStatutCommande().equals("reçue")){
                reçu.add(commande);
            }
        }
        return reçu;
    }
    //nbr de commande reçu
    @GetMapping("/NbrcommandesReçu")
    public int getNbrCommandesReçu() {
        Statut statut = statutRepository.findById(1).orElse(new Statut());
        List<Commande>  commandes = cmdRepository.findAll();
        int count =0;
        for (Commande commande : commandes) {
            if (commande.getStatut().getStatutCommande().equals("reçue")){
               count++;
            }
        }
        return count;
    }




}
