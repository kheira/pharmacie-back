package com.example.Pharmacie.repository;

import com.example.Pharmacie.model.Medicament;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MedicamentRepository extends MongoRepository<Medicament, Integer> {
}
