package com.example.Pharmacie.repository;


import com.example.Pharmacie.model.Commande;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommandeRepository extends MongoRepository<Commande, Integer> {
}
