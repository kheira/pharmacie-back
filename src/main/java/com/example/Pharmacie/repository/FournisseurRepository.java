package com.example.Pharmacie.repository;

import com.example.Pharmacie.model.Fournisseur;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FournisseurRepository extends MongoRepository<Fournisseur, Integer> {
}
