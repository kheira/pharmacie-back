package com.example.Pharmacie.repository;

import com.example.Pharmacie.model.Statut;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StatutRepository extends MongoRepository<Statut, Integer> {

}

