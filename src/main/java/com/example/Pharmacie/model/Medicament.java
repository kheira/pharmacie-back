package com.example.Pharmacie.model;


import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.ManyToOne;
import java.util.Date;

@Document
public class Medicament {

    private String codeBarre;
    private String nom;
    private int quantite;
    private Date datePeremption;
    private float prix;
    private Fournisseur fournisseur;


    public Medicament() {
        super();
    }
    public Medicament( String codeBarre, String nom, int quantite, Date datePeremption){
        super();
        this.codeBarre = codeBarre;
        this.nom = nom;
        this.quantite = quantite;
        this.datePeremption = datePeremption;

    }

    public String getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(String codeBarre) {
        this.codeBarre = codeBarre;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Date getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(Date datePeremption) {
        this.datePeremption = datePeremption;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    @ManyToOne
    public Fournisseur getFournisseur() {
        return fournisseur;
    }
    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }
}
