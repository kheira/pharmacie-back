package com.example.Pharmacie.model;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Document
public class Statut {

    private int idStatut;
    private String statutCommande;

    public Statut() {
        super();
    }


    public Statut(int idStatut, String statutCommande) {
        this.idStatut = idStatut;
        this.statutCommande = statutCommande;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    public int getIdStatut() {
        return idStatut;
    }

    public void setIdStatut(int idStatut) {
        this.idStatut = idStatut;
    }

    public String getStatutCommande() {
        return statutCommande;
    }

    public void setStatutCommande(String statutCommande) {
        this.statutCommande = statutCommande;
    }
}
