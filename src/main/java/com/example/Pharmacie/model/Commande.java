package com.example.Pharmacie.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
public class Commande {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int idCommande;
    private int nbrMed;
    private float montant;
    private Date dateCommande;
    private Medicament medicament;
    private Statut statut;



    public Commande() {
        super();
    }

    public Commande(int idCommande, int nbrMed, int montant, Date dateCommande) {
        super();
        this.idCommande = idCommande;
        this.nbrMed = nbrMed;
        this.montant = montant;
        this.dateCommande = dateCommande;
    }


    public int getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(int idCommande) {
        this.idCommande = idCommande;
    }

    public int getNbrMed() {
        return nbrMed;
    }

    public void setNbrMed(int nbrMed) {
        this.nbrMed = nbrMed;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }


    @ManyToOne
    public Medicament getMedicament() {
        return medicament;
    }
    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }
    @ManyToOne
    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }


}
