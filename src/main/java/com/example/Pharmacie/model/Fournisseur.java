package com.example.Pharmacie.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Document
public class Fournisseur {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    private int idFournisseur;
    private String nomFournisseur;
    private String mail;


    public Fournisseur(int idFournisseur, String nomFournisseur, String mail) {
        this.idFournisseur = idFournisseur;
        this.nomFournisseur = nomFournisseur;
        this.mail = mail;

    }

    public Fournisseur() {
       super();
    }


    public int getIdFournisseur() {
        return idFournisseur;
    }

    public void setIdFournisseur(int idFournisseur) {
        this.idFournisseur = idFournisseur;
    }

    public String getNomFournisseur() {
        return nomFournisseur;
    }

    public void setNomFournisseur(String nomFournisseur) {
        this.nomFournisseur = nomFournisseur;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }


}
